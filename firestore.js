const cafeList = document.getElementById('cafe-list');
const form = document.getElementById('add-cafe-form');



//TODO : Render Cafe List 
const renderCafe = (cafe) => {
    let li = document.createElement('li');
    let name = document.createElement('span');
    let city = document.createElement('span');
    let cross = document.createElement('div');
    li.setAttribute('data-id', cafe.id);
    name.textContent = cafe.data().name;
    city.textContent = cafe.data().city;
    cross.textContent = 'x';

    li.appendChild(name);
    li.appendChild(city);
    li.appendChild(cross);

    cafeList.appendChild(li);

    cross.addEventListener('click', (event) => {
        event.stopPropagation();
        let id = event.target.parentElement.getAttribute('data-id');

        db.collection('cafes').doc(id).delete();
        loadCafe();
        alert('Item deleted!');
    })
}



//TODO : Get Cafe From Database

const getAll = async() => {
    const cafes = await db.collection('cafes').get();
    return cafes;
}



//TODO : Add Cafe To Database Via Form Submit
form.addEventListener('submit', (event) => {
    event.preventDefault();
    let name = form.name.value;
    let city = form.city.value;

    db.collection('cafes').add({
        name,
        city
    });

    loadCafe();

})

//TODO: Load Cafe List 
const loadCafe = async() => {
    cafeList.innerHTML = '';
    const cafes = await getAll();

    cafes.forEach((cafe) => {
        renderCafe(cafe);
    });
}

window.onload = loadCafe();